/* Bridge Search Server
 * sever.js
 *
 * Entry point for the ElasticSearch request*/

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
//var cors = require('cors');

//app.use(cors({credentials: false}));
//app.options('*', cors({credentials: false}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended:true }));

app.use('/search', require('./routes/tags.js'));
app.use('/search', require('./routes/messages.js'));
app.use('/bridges', require('./routes/bridges.js'));

app.set('port', (process.env.PORT || 8090));
app.listen(app.get('port'), function() {
	'use strict';

  console.log('Node app is running on port ' + app.get('port'));
});
