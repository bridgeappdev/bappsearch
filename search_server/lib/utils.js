/*
 * Utils librabry for bridgeSearchServer
 * */
//jshint node:true
var whitelist = [
	'http://app.chakachat.com',
	'https://app.chakachat.com',
	'http://www.local.bridgeapp.me'
];

module.exports.corsOptions = {
	credentials: true,
  origin: function(origin, callback){
		'use strict';

    var originIsWhitelisted = whitelist.indexOf(origin) !== -1;
    callback(null, originIsWhitelisted);
  }
};
