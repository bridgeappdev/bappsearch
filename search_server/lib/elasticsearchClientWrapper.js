var elasticsearch = require('elasticsearch');

// initialize our ElasticSearch API
var client = new elasticsearch.Client({
	host: '127.0.0.1:9200',
	//host: '192.241.170.241:9200',
	log: 'trace'
});

module.exports = client;
