//jshint node:true

var router = require('express').Router(),
		cors = require('cors'),
		client = require('../lib/elasticsearchClientWrapper.js'),
		searchConfig = { "bridgeIndex": "bappbridges", "bridgeType": {"chat":"chatbridges", "event":"eventbridges"}}; 

function arrayToObject(array){
	'use strict';

	var length = array.length;
	var obj = {};
	if(array){
		for (var i=0;i<length;i++){
			var nextItem = array[i];
			obj[nextItem] = true; 
		}
	}
	return obj;
}

var findMyBridges = function (req, res){
	'use strict';

	var index = searchConfig.bridgeIndex;
	var data = req.body; 
	var userID = data.userID; 

	var query = {
		index: index,
		body: {
			query: {
				filtered: {
					query : {
						match_all : {}	
					},
					filter: {
						term: {
							members: userID
						}
					}
				}
			}
		}
	};
	
	client.search(query, function (error, results) {
		if(error){
			console.trace(error.message);
		}else{
			var hits = results.hits.hits;
			var responseData = {};
			var noOfhits = hits.length; 
			for (var i=0;i<noOfhits;i++){
				var source =  hits[i]._source;
				source.members = arrayToObject(source.members);
				responseData[hits[i]._id] = source;
			}
		
			res.send(responseData);
		}
	});
}; 

// middleware specific to this router
router.use(function timeLog(req, res, next) {
	'use strict';

  console.log('Time: ', Date.now());
  next();
});

var corsOptions = require('../lib/utils.js').corsOptions; 

router.options('/byuid/', cors(corsOptions));
router.post('/byuid/', cors(corsOptions), findMyBridges);
module.exports = router;

