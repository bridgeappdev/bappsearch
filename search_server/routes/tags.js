
var express = require('express'),
		cors = require('cors'),
		router = express.Router(),
		client = require('../lib/elasticsearchClientWrapper.js'),
		index = "bridgeapp",
		searchConfig = {
			"tagIndex": "bapptags",
		 	"tagType": "tags",
			"bridgeIndex": "bappbridges",
		 	"bridgeType": {
				"chat":"chatbridges",
				"event":"eventbridges"
			}
		};

var findBridgesByTags = function (req, res){
	'use strict';

	var data = req.body;
	var tags = data.query;
	var noOfTags = tags.length;
	var shouldClause= [];

	for (var i=0; i<noOfTags;i++){
		var termQuery = {term: { tags: {}}};
		termQuery.term.tags = tags[i];
		shouldClause.push(termQuery);
	}

	var query = {
		index: data.index,
		body: {
			query:{
				bool: {
					should: shouldClause,
					minimum_should_match: 1
				}
			}
		}
	};

	client.search(query, function (error, results) {
		if(error){
			console.trace(error.message);
		}else{
			var hits = results.hits.hits;
			var responseData = {};
			var noOfhits = hits.length;
			for (var i=0;i<noOfhits;i++){
				responseData[hits[i]._id] = hits[i]._source;
			}

			res.send(responseData);
		}
	});
};

//HERE
var suggestTags = function (req, res){
	'use strict';

	var queryBody = {
		"query": {
			"bool": {
				"should": [
					{
						"match": {
							"name": req.body.text
						}
					},
					{
						"bool": {
							"should": [
								{
									"match": {
										"desc": req.body.text
									}
								}
	          	]
	        	}
					}
	     	]
	    }
	  }
	};

	client.search({
	index: searchConfig.tagIndex,
	type: searchConfig.tagType,
	size: 6,
	body: queryBody
	}, function (error, response) {
		if(error){
			console.trace(error.message);
		}else{
			var responseData = [];
			var hits = response.hits.hits;
			var hitsLength =hits.length;

			for (var i=0;i<hitsLength;i++){
				responseData.push(hits[i]._source);
			}

			res.send(JSON.stringify(responseData));
		}

	});
};

// middleware specific to this router
router.use(function timeLog(req, res, next) {
	'use strict';

  console.log('Time: ', Date.now());
  next();
});

var corsOptions = require('../lib/utils.js').corsOptions;

router.options('/tags/_suggest', cors(corsOptions));
router.options('/tags', cors(corsOptions));
router.post('/tags', cors(corsOptions), findBridgesByTags);
router.post('/tags/_suggest', cors(corsOptions), suggestTags);
module.exports = router;
