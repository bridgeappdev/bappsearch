'use strict';

var express = require('express'),
		cors = require('cors'),
		router = express.Router(),
		client = require('../lib/elasticsearchClientWrapper.js'),
		searchConfig = {
			"messageIndex": "ccmessages",
		 	"messageType": {
				"type-message":"chatmessages",
				"chat":"chatmessages",
				"file":"filemessages",
				"post": "postmessages"
			}
		};

var searchMessgeContent = function (req, res){

	var data = req.body;
	var searchPhrase = data.query;
	var bridgeIds = data.bridges;
	var messageTypes = data.types;
	var types;
	if(messageTypes){
		for (var i=0;i<messageTypes.length;i++){
			if(!types){
				types = searchConfig.messageType[messageTypes[i]];
			}else{
				types += (',' + searchConfig.messageType[messageTypes[i]]);
			}
		}
	}
	var shouldClauseBridges = [];
	var shouldClauseTypes = [];

	var query = {
		index: searchConfig.messageIndex,
		type: types,
		body: {
			query:{
				bool:{
					must:{
						match: {
							message: searchPhrase
						}
					},
					filter:[
						{
							terms: {
								bridgeid: bridgeIds
							}
						}
					]
				}
			},
			highlight: {
				fields : {
					message : {}
				}
			}
		}
	};

	var responseData = {};
	client.search(query, function (error, results) {
		if(error){
			console.trace(error.message);
			return res.status(500).send(
				{
					type:"error",
					data:{},
					message:"Error fulfilling your request"
				}
			)
		}else{
			var hits = results.hits.hits;
			var noOfhits = hits.length;
			for (var i=0;i<noOfhits;i++){
				responseData[hits[i]._id] = hits[i]._source;
				responseData[hits[i]._id].highlight = hits[i].highlight.message[0];
			}
			return res.send(
				{
					type:"content_search",
					data:{messages: responseData}
				}
			);
		}
	});

};

var corsOptions = require('../lib/utils.js').corsOptions;

router.options('/messages', cors(corsOptions));
router.post('/messages', cors(corsOptions), searchMessgeContent);
module.exports = router;
