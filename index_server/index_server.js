'use strict';

var Firebase = require('firebase');
var elasticsearch = require('elasticsearch');
// initialize our ElasticSearch API
var client = new elasticsearch.Client({
	host: '127.0.0.1:9200',
	//host: '192.241.170.241:9200',
	log: 'trace'
});

var tagIndex = 'bapptags',
		bridgeIndex = 'bappbridges',
		messageIndex = 'ccmessages';

// TODO Move this to an environment variable.
var AUTH_TOKEN = '08Bmt9Y2qQQ1qHuWoDz8moVKWAQZZdpEZsKsajR2';

var fb = new Firebase('https://br1dg3co.firebaseio.com/');
fb.authWithCustomToken(AUTH_TOKEN, function(error, authData) {
  if (error) {
    console.log("Login Failed!", error);
  } else {
    console.log("Authenticated successfully with payload:", authData);

		/*var tagDocRef = fb.child('tags');
		tagDocRef.on('child_added',   createOrUpdateTagIndex);
		tagDocRef.on('child_changed', createOrUpdateTagIndex);
		tagDocRef.on('child_removed', removeTagIndex);
*/
		var bridgeDocRef = fb.child('bridges');
		bridgeDocRef.on('child_added',   newBridgeIndex);
		bridgeDocRef.on('child_changed', createOrUpdateBridgeIndex);
		bridgeDocRef.on('child_removed', removeBridgeIndex);
	}
});

var addMessageListeners = function addMessageListeners(bridgeId){
	var messagesDocRef = fb.child('messages').child(bridgeId);
	messagesDocRef.on('child_added',   createOrUpdateMessageIndex);
	messagesDocRef.on('child_changed', createOrUpdateMessageIndex);
	messagesDocRef.on('child_removed', removeMessageIndex);
};

//TAGS
var createTagDocument	= function createTagDocument(tagSnapshot){

	var input = tagSnapshot.val();
		return {
			"name" : tagSnapshot.key(),
			"desc": input.desc,
			"count": input.count
		};
};

var createOrUpdateTagIndex	=	function createOrUpdateTagIndex(snap) {

	console.log("Got tag data ", snap.val());
	client.index({
		index: tagIndex,
		type: "tags",
		id: snap.key(),
		body: createTagDocument(snap)

	}, function (error, response) {
	if(error){
		console.log('Error: ', error);
	}else{
		console.log('indexed ', snap.key());
	}
	});
}

var removeTagIndex = function removeTagIndex(snap) {

	client.delete({
		index: index,
		type: 'tags',
		id: snap.key()
	}, function (error, response) {
	      if( error ) {console.error('failed to delete', snap.key(), error);
		}else{
				console.log('deleted', snap.key());
			}
		});
}

//BRIDGES
var newBridgeIndex = function createBridgeIndex(snap){
	addMessageListeners(snap.key());
	createOrUpdateBridgeIndex(snap);
};

var createOrUpdateBridgeIndex = function createOrUpdateBridgeIndex(snap) {

	var _source = snap.val(),
				type;
	switch (_source.type){
		case 'chat':
			type = 'chatbridges';
			break;
		case 'event':
			type = 'eventbridges';
	}
	var membersarray = [];
	if(_source.members) {
		membersarray = Object.keys(_source.members);
	}
	_source.members = membersarray;

	client.index({
		refresh: true,
		index: bridgeIndex,
		type: type,
		id: snap.key(),
		body: _source

	}, function (error, response) {
		if(error){
			console.log('Error: ', error);
		}else{
			console.log('indexed ', snap.key());
		}
	});
}

var removeBridgeIndex = function removeBridgeIndex(snap) {

	var type;
	switch (snap.val().type){
		case 'chat':
		type = 'chatbridges';
		break;
		case 'event':
		type = 'eventbridges';
	}
	client.delete({
		index: bridgeIndex,
		type: type,
		id: snap.key()
	}, function (error, response) {
		if( error ) {console.error('failed to delete', snap.key(), error);
	}else{
		console.log('deleted', snap.key());
	}
});
}

//MESSAGES
var createOrUpdateMessageIndex = function createOrUpdateMessageIndex(snap) {
//TODO: Use bulk update as we get all messages for a bridge.
	var _source = snap.val(),
			type;
	switch (_source.type){
		case 'chat':
			type = 'chatmessages';
			break;
		case 'post':
			type = 'postmessages';
			break;
		case 'file':
			type = 'filemessages';
			break;
		default:
			type = 'chatmessages';
	}
	_source.bridgeid = snap.ref().parent().key();
	client.index({
		refresh: true,
		index: messageIndex,
		type: type,
		id: snap.key(),
		body: _source
	}, function (error, response) {
		if(error){
			console.log('Error: ', error);
		}else{
			console.log('indexed ', snap.key());
		}
	});
}

var removeMessageIndex = function removeMessageIndex(snap) {

	var type;
	switch (snap.val().type){
		case 'chat':
			type = 'chatmessages';
			break;
		case 'post':
			type = 'postmessages';
			break;
		case 'file':
			type = 'filemessages';
			break;
		default:
			type = 'chatmessages';
	}
	client.delete({
		index: messageIndex,
		type: type,
		id: snap.key()
	}, function (error, response) {
		if( error ) {console.error('failed to delete', snap.key(), error);
			}else{
				console.log('deleted', snap.key());
			}
	});
}
