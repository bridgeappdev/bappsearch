####TAGS####

curl -XPUT "http://127.0.0.1:9200/bapptags" -d'
{
  "settings": {
    "number_of_shards": 1,
    "analysis": {
      "filter": {
        "autocomplete_filter": {
          "type": "edge_ngram",
          "min_gram": 1,
          "max_gram": 20
        }
      },
      "analyzer": {
        "autocomplete": {
          "type": "custom",
          "tokenizer": "standard",
          "filter": [
          "lowercase",
          "autocomplete_filter"
          ]
        }
      }
    }
  }
}'

curl -XPUT "http://127.0.0.1:9200/bapptags/_mapping/tags" -d'
{
  "tags": {
    "properties": {
      "name": {
        "type": "string",
        "analyzer": "autocomplete",
        "search_analyzer": "standard"
      },
      "desc": {
        "type": "string"
      },
      "count":{
        "type":"integer"
      }
    }
  }
}'

####BRIDGES####

curl -XPUT "http://127.0.0.1:9200/bappbridges" -d'
{
  "settings": {
    "number_of_shards": 1
  }
}'

curl -XPUT "http://127.0.0.1:9200/bappbridges/_mapping/chatbridges" -d'
{
  "chatbridges": {
    "properties": {
      "createdAt": {
        "type": "long"
      },
      "desc": {
        "type": "string"
      },
      "members": {
        "type": "string",
        "index": "not_analyzed"
      },
      "name": {
        "type": "string"
      },
      "tags": {
        "type": "string"
      },
      "title": {
        "type": "string"
      },
      "type": {
        "type": "string"
      },
      "visibility": {
        "type": "string"
      }
    }
  }
}'

curl -XPUT "http://127.0.0.1:9200/bappbridges/_mapping/eventbridges" -d'
{
  "eventbridges": {
    "properties": {
      "createdAt": {
        "type": "long",
        "index": "not_analyzed"
      },
      "data_and_time": {
        "type": "object",
        "properties": {
          "enddate": {
            "type": "date",
            "index": "not_analyzed"
          },
          "endtime": {
            "type": "date",
            "index": "not_analyzed"
          },
          "startdate": {
            "type": "date",
            "index": "not_analyzed"
          },
          "starttime": {
            "type": "date",
            "index": "not_analyzed"
          }
        }
      },
      "desc": {
        "type": "string"
      },
      "ebdata": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "index": "no"
          },
          "organiser": {
            "type": "object",
            "properties": {
              "description": {
                "type": "object",
                "properties": {
                  "html": {
                    "type": "string",
                    "index": "no"
                  },
                  "text": {
                    "type": "string",
                    "index": "no"
                  }
                }
              },
              "id": {
                "type": "string",
                "index": "no"
              },
              "name": {
                "type": "string"
              },
              "num_future_events": {
                "type": "integer",
                "index": "no"
              },
              "num_past_events": {
                "type": "integer",
                "index": "no"
              },
              "resource_uri": {
                "type": "string",
                "index": "no"
              },
              "url": {
                "type": "string",
                "index": "no"
              }
            }
          },
          "url": {
            "type": "string",
            "index": "no"
          }
        }
      },
      "event_image_url": {
        "type": "string",
        "index": "no"
      },
      "members": {
        "type": "string",
        "index": "not_analyzed"
      },
      "name": {
        "type": "string"
      },
      "tags": {
        "type": "string"
      },
      "title": {
        "type": "string"
      },
      "type": {
        "type": "string"
      },
      "venue": {
        "type": "object",
        "properties": {
          "venue1": {
            "type": "string",
            "index": "no"
          },
          "venue2": {
            "type": "string",
            "index": "no"
          },
          "venue3": {
            "type": "string",
            "index": "no"
          },
          "venue4": {
            "type": "string",
            "index": "no"
          }
        }
      },
      "visibility": {
        "type": "string"
      }
    }
  }
}'

###MESSAGES###

curl -XPUT "http://127.0.0.1:9200/ccmessages" -d'
{
  "settings": {
    "number_of_shards": 1
  }
}'
curl -XPUT "http://127.0.0.1:9200/ccmessages/_mapping/chatmessages" -d'
{
  "chatmessages": {
    "properties": {
      "bridgeid": {
        "type": "string",
        "index": "not_analyzed"
      },
      "timestamp": {
        "type": "date",
        "format": "epoch_millis"
      },
      "user": {
        "type": "string"
      },
      "userId": {
        "type": "string",
        "index": "not_analyzed"
      },
      "message": {
        "type": "string"
      },
      "avatarurl": {
        "type": "string",
        "index": "no"
      },
      "type": {
        "type": "string"
      }
    }
  }
}'
curl -XPUT "http://127.0.0.1:9200/ccmessages/_mapping/filemessages" -d'
{
  "filemessages": {
    "properties": {
      "bridgeid": {
        "type": "string",
        "index": "not_analyzed"
      },
      "timestamp": {
        "type": "date",
        "format": "epoch_millis"
      },
      "user": {
        "type": "string"
      },
      "userId": {
        "type": "string",
        "index": "not_analyzed"
      },
      "message": {
        "type": "string"
      },
      "avatarurl": {
        "type": "string",
        "index": "no"
      },
      "type": {
        "type": "string"
      }
    }
  }
}'
curl -XPUT "http://127.0.0.1:9200/ccmessages/_mapping/postmessages" -d'
{
  "postmessages": {
    "properties": {
      "bridgeid": {
        "type": "string",
        "index": "not_analyzed"
      },
      "timestamp": {
        "type": "date",
        "format": "epoch_millis"
      },
      "user": {
        "type": "string"
      },
      "userId": {
        "type": "string",
        "index": "not_analyzed"
      },
      "message": {
        "type": "string"
      },
      "avatarurl": {
        "type": "string",
        "index": "no"
      },
      "type": {
        "type": "string"
      }
    }
  }
}'
